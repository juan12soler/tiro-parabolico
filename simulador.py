from math import *
from time import sleep

from graphics import *

v0=60
t=0
w0=60*3.1415926535/180

y=0
g=9.8
x=0

ventana=GraphWin("simulador",1000,1000)
ventana.setCoords(0,0,400,400)

while(y>=0):
    x=v0*cos(w0)*t
    y=v0*sin(w0)*t-(1.0/2)*g*t*t
    miCirculo=Circle(Point(x,y),10)
    miCirculo.setFill('yellow')
    miCirculo.draw(ventana)
    print(x,y)
    sleep(0.1)
    t=t+0.1

ventana.getMouse()
